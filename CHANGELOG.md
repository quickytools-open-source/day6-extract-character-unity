# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.0.7] - 2021-06-17
### Added
- Material (standard or URP) is set for character colors.
- Prompt about unsaved scene changes before generating character scene.

## [0.0.6] - 2021-06-05
### Added
- Extracts characters into a prefab.
- Generates a demo scene.
- Auto extract assets placed in specific folder (Assets/quickytools/Day6/AutoExtract).