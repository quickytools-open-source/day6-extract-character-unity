using UnityEngine;

namespace Day6
{
    [RequireComponent(typeof(Animator))]
    public class Day6CharacterController : MonoBehaviour
    {
        private Animator animator;

        private readonly int HashForwardSpeed = Animator.StringToHash("ForwardSpeed");

        private void OnEnable()
        {
            animator = GetComponent<Animator>();

            CharacterInputEvents.MoveCharacterEvent += OnMoveCharacter;
        }

        private void OnDisable()
        {
            CharacterInputEvents.MoveCharacterEvent -= OnMoveCharacter;
        }

        private void OnMoveCharacter(Vector2 moveInput)
        {
            var forwardSpeed = Mathf.Max(0, moveInput.y);
            animator.SetFloat(HashForwardSpeed, forwardSpeed);
        }
    }
}