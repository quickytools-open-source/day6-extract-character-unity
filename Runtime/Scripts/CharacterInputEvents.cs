using System;
using UnityEngine;

namespace Day6
{
    public static class CharacterInputEvents
    {
        public static Action<Vector2> MoveCharacterEvent;
    }
}