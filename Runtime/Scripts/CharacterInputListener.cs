using UnityEngine;
using UnityEngine.InputSystem;

namespace Day6
{
    public class CharacterInputListener : MonoBehaviour
    {
        private void OnMove(InputValue value)
        {
            var inputVector = value.Get<Vector2>();
            CharacterInputEvents.MoveCharacterEvent?.Invoke(inputVector);
        }
    }
}