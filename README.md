# Character Extract

Extracts Day6 characters into prefabs for use in Unity. A sample scene can also be generated with the character prefab.

Package git URL is `https://gitlab.com/quickytools-open-source/day6-extract-character-unity.git`.

Create a directory at Assets/quickytools/Day6/AutoExtract and drag character assets into it to run extraction automatically.