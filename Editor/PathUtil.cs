using System.IO;
using UnityEngine;

namespace Day6.Editor
{
    public static class PathUtil
    {
        public const string CharacterImportRootDir = "Assets/quickytools/Day6/AutoExtract";

        public static (string, string, string) GetFilePathName(string path)
        {
            var lastSlash = path.LastIndexOf("/");
            var dirPath = path.Substring(0, lastSlash);
            var fileName = path.Substring(lastSlash + 1);
            var baseName = Path.GetFileNameWithoutExtension(fileName);
            return (dirPath, fileName, baseName);
        }

        public static (string, string, string) GetExtractDirPath(string assetPath)
        {
            var (dirPath, fileName, baseName) = PathUtil.GetFilePathName(assetPath);

            var extractDirName = $"{baseName}-extract";
            var extractAssetPath = $"{dirPath}/{extractDirName}";
            return (fileName, baseName, extractAssetPath);
        }

        public static string GetAnimationControllerPath()
        {
            var notFoundMessage = "Day6 animator controller not found. See instructions for location of necessary extraction files.";
            var animatorControllerAssetPath = AssetUtil.GetFirstAssetPath("Day6 t:AnimatorController", notFoundMessage);
            return animatorControllerAssetPath;
        }

        public static string GetAbsPath(string assetPath) => $"{Application.dataPath}/{assetPath}".Replace("Assets/Assets", "Assets");
    }
}