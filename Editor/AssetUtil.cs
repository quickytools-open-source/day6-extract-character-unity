using UnityEditor;

namespace Day6.Editor
{
    public static class AssetUtil
    {
        public static string GetFirstAssetPath(string query, string notFoundMessage = "")
        {
            string assetPath = "";

            var animatorControllerGuids = AssetDatabase.FindAssets(query);
            if (animatorControllerGuids.Length == 0)
            {
                if (!string.IsNullOrEmpty(notFoundMessage))
                {
                    EditorUtility.DisplayDialog("File not found", notFoundMessage, "OK");
                }
            }
            else
            {
                // TODO Select the correct controller if there is more than 1
                var guid = animatorControllerGuids[0];
                assetPath = AssetDatabase.GUIDToAssetPath(guid);
            }

            return assetPath;
        }
    }
}