using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Rendering;
using System.IO;

namespace Day6.Editor
{
    public class ExtractDay6Character : MonoBehaviour
    {
        [MenuItem("Tools/quickytools/Day6/Extract character")]
        public static void ExtractCharacter()
        {
            ExtractCharacter(false);
        }

        [MenuItem("Tools/quickytools/Day6/Extract character - no prompts")]
        public static void ExtractCharacterNoPrompt()
        {
            ExtractCharacter(true);
        }

        private static void ExtractCharacter(bool force)
        {
            var fbxAssetFiles = GetSelectedAssets();
            var animatorControllerAssetPath = PathUtil.GetAnimationControllerPath();
            if (fbxAssetFiles.Count > 0 && !string.IsNullOrEmpty(animatorControllerAssetPath))
            {
                ExtractCharacterAssets(fbxAssetFiles, animatorControllerAssetPath, force);
            }
        }

        private static IList<string> GetSelectedAssets()
        {
            var fbxAssetFiles = new List<string>();
            foreach (var obj in Selection.GetFiltered(typeof(Object), SelectionMode.Assets))
            {
                var path = AssetDatabase.GetAssetPath(obj);
                if (!string.IsNullOrEmpty(path) && File.Exists(path) && Path.GetExtension(path) == ".fbx")
                {
                    fbxAssetFiles.Add(path);
                }
            }

            if (fbxAssetFiles.Count == 0)
            {
                EditorUtility.DisplayDialog("Select files", "Select Day6 character asset files to import", "OK");
            }

            return fbxAssetFiles;
        }

        private static readonly ISet<string> LoopingAnimationsNames = new HashSet<string> { "idle", "walk" };

        private static bool IsLoopingAnimation(AnimationClip clip)
        {
            var name = clip.name.ToLower();
            foreach (var loopingName in LoopingAnimationsNames)
            {
                if (name.Contains(loopingName))
                {
                    return true;
                }
            }
            return false;
        }

        private static IList<AnimationClip> GetAssetAnimations(string assetPath)
        {
            // Reference animation clips
            var subAssets = AssetDatabase.LoadAllAssetRepresentationsAtPath(assetPath);
            var animations = new List<AnimationClip>();
            foreach (Object o in subAssets)
            {
                if (o is AnimationClip clip)
                {
                    animations.Add(clip);
                }
            }
            return animations;
        }

        private static (string, string, string, string) SetDirectoryStructure(string assetPath, bool force)
        {
            var (fileName, baseName, extractAssetPath) = PathUtil.GetExtractDirPath(assetPath);
            var extractDirAbsPath = $"{Application.dataPath}/{extractAssetPath}".Replace("Assets/Assets", "Assets");

            var secondaryDirName = "base-assets";
            var secondaryAssetPath = $"{extractAssetPath}/{secondaryDirName}";

            if (Directory.Exists(extractDirAbsPath) && !force)
            {
                var message = $"Contents in directory {extractAssetPath} will be overwritten.\nIf changes were made that need preserving press Cancel and move contents then extract again.";
                var overwrite = EditorUtility.DisplayDialog("Overwrite", message, "Continue", "Cancel");
                if (!overwrite)
                {
                    return ("", "", "", "");
                }
            }

            var dirAbsPaths = new string[] { extractDirAbsPath, $"{extractDirAbsPath}/{secondaryDirName}" };
            foreach (var dirAbsPath in dirAbsPaths)
            {
                if (!Directory.Exists(dirAbsPath))
                {
                    Directory.CreateDirectory(dirAbsPath);
                }
            }

            return (fileName, baseName, extractAssetPath, secondaryAssetPath);
        }

        internal static void ExtractCharacterAssets(IList<string> assetPaths, string animatorControllerAssetPath, bool force = false)
        {
            foreach (var assetPath in assetPaths)
            {
                ExtractCharacter(assetPath, animatorControllerAssetPath, force);
            }
        }

        private static void ExtractCharacter(string assetPath, string animatorControllerAssetPath, bool force = false)
        {
            // Set directory structure
            var (fileName, baseName, extractAssetPath, secondaryAssetPath) = SetDirectoryStructure(assetPath, force);
            if (string.IsNullOrEmpty(fileName))
            {
                return;
            }

            var importer = ModelImporter.GetAtPath(assetPath) as ModelImporter;
            // Convert Units
            importer.useFileScale = false;

            // Humanoid animation type
            importer.animationType = ModelImporterAnimationType.Human;

            // Reference animation clips
            var originalAnimations = GetAssetAnimations(assetPath);

            // Apply changes
            importer.SaveAndReimport();

            // Extract animation clips
            var copyAnimations = new List<AnimationClip>();
            foreach (var animation in originalAnimations)
            {
                var animationCopy = new AnimationClip();
                EditorUtility.CopySerialized(animation, animationCopy);
                if (IsLoopingAnimation(animation))
                {
                    var settings = AnimationUtility.GetAnimationClipSettings(animationCopy);
                    settings.loopTime = true;
                    settings.loopBlend = true;
                    AnimationUtility.SetAnimationClipSettings(animationCopy, settings);
                }
                AssetDatabase.CreateAsset(animationCopy, $"{secondaryAssetPath}/{animation.name}.anim");
                copyAnimations.Add(animationCopy);
            }

            // Create copy without animation clips
            var assetCopyPath = $"{secondaryAssetPath}/{fileName}";
            AssetDatabase.CopyAsset(assetPath, assetCopyPath);
            importer = ModelImporter.GetAtPath(assetCopyPath) as ModelImporter;
            importer.importAnimation = false;
            importer.SaveAndReimport();

            // Create minimal character prefab
            var mainAsset = AssetDatabase.LoadMainAssetAtPath(assetCopyPath);
            var characterObject = Instantiate(mainAsset, Vector3.zero, Quaternion.identity) as GameObject;

            UpdateMesh(characterObject);

            characterObject.AddComponent<Day6CharacterController>();
            var characterPrefabPath = $"{extractAssetPath}/{baseName}.prefab";
            PrefabUtility.SaveAsPrefabAsset(characterObject, characterPrefabPath);
            DestroyImmediate(characterObject.gameObject);

            AssetDatabase.SaveAssets();

            // Create animator override controller
            var overrideController = CreateAnimatorOverrideController(animatorControllerAssetPath, copyAnimations);
            if (overrideController == null)
            {
                EditorUtility.DisplayDialog("Incomplete extraction", "Animator override controller not created. Issue during extraction.", "OK");
                return;
            }

            var overrideControllerAssetPath = $"{secondaryAssetPath}/{baseName}.overrideController";
            AssetDatabase.CreateAsset(overrideController, overrideControllerAssetPath);
            AssetDatabase.SaveAssets();

            // Set animator controller on character prefab
            if (!string.IsNullOrEmpty(overrideControllerAssetPath))
            {
                var characterPrefab = AssetDatabase.LoadAssetAtPath<GameObject>(characterPrefabPath);
                var animatorController = AssetDatabase.LoadAssetAtPath<RuntimeAnimatorController>(overrideControllerAssetPath);
                characterPrefab.GetComponent<Animator>().runtimeAnimatorController = animatorController;
                EditorUtility.SetDirty(characterPrefab);
                AssetDatabase.SaveAssets();
            }
        }

        private static void UpdateMesh(GameObject characterObject)
        {
            SkinnedMeshRenderer meshRenderer = null;
            Transform rootBone = null;
            foreach (Transform child in characterObject.transform)
            {
                var renderer = child.GetComponent<SkinnedMeshRenderer>();
                if (renderer != null)
                {
                    if (renderer.rootBone.name.ToLower().Contains("root"))
                    {
                        break;
                    }
                    meshRenderer = renderer;
                }
                else if (child.childCount == 1)
                {
                    var depth = 0;
                    var transform = child;
                    while (rootBone == null && transform != null && depth++ < 3)
                    {
                        if (transform.name.ToLower().Contains("root"))
                        {
                            rootBone = transform;
                            break;
                        }
                        else
                        {
                            transform = transform.GetChild(0);
                        }
                    }
                }
            }

            if (meshRenderer != null)
            {
                if (rootBone != null)
                {
                    // TODO Fix this on backend rather than here
                    meshRenderer.rootBone = rootBone;
                }

                SetMeshMaterial(meshRenderer);
            }
        }

        private static void SetMeshMaterial(SkinnedMeshRenderer meshRenderer)
        {
            var renderingPipeline = GraphicsSettings.renderPipelineAsset?.name ?? "";
            var materialName = "Day6VertexColorStandardMaterial";
            switch (renderingPipeline)
            {
                case "UniversalRenderPipelineAsset":
                    materialName = "Day6VertexColorUrpMaterial";
                    break;
                // TODO HDRP
                default:
                    break;
            }
            var materialAssetPath = AssetUtil.GetFirstAssetPath($"{materialName} t:Material");
            if (!string.IsNullOrEmpty(materialAssetPath))
            {
                var material = AssetDatabase.LoadAssetAtPath<Material>(materialAssetPath);
                meshRenderer.material = material;
            }
        }

        private static AnimatorOverrideController CreateAnimatorOverrideController(string animatorControllerAssetPath, IList<AnimationClip> animations)
        {
            var baseAnimatorController = AssetDatabase.LoadAssetAtPath<RuntimeAnimatorController>(animatorControllerAssetPath);
            var overrideController = new AnimatorOverrideController
            {
                runtimeAnimatorController = baseAnimatorController
            };
            var overrideAnimationClips = new List<KeyValuePair<AnimationClip, AnimationClip>>();
            var overridingClips = new Dictionary<string, AnimationClip>();
            foreach (var animationClip in animations)
            {
                overridingClips[animationClip.name.ToLower().Replace("-", "_")] = animationClip;
            }
            foreach (var animationClip in overrideController.animationClips)
            {
                // TODO Enforce naming convention
                var clipName = animationClip.name.ToLower().Replace("day6-", "").Replace("-", "_");
                // TODO Enforce uniqueness or match above from longest to shortest in length.
                //      Maybe hard code a lookup table and fall back to fuzzy matching.
                var matchingKey = "";
                foreach (var entry in overridingClips)
                {
                    if (entry.Key.Contains(clipName))
                    {
                        matchingKey = entry.Key;
                        overrideAnimationClips.Add(new KeyValuePair<AnimationClip, AnimationClip>(animationClip, entry.Value));
                        break;
                    }
                }
                if (!overridingClips.ContainsKey(matchingKey))
                {
                    return null;
                }
                else
                {
                    overridingClips.Remove(matchingKey);
                }
            }
            overrideController.ApplyOverrides(overrideAnimationClips);
            return overrideController;
        }
    }
}